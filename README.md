Arena SECC OAuth Data Library
===============================================================================
This project is for building an assembly for the OAuth datalayer.

Setup
-----
In order to setup this project in Arena, follow these steps:
* Create the database tables from OAuth.sql
* Build the project
* Drop the resulting Arena.Custom.SECC.Oauth.dll file into your Arena bin directory

Building a NuGet Package
-----
In a command prompt, cd to the root of the project (the directory with the .sln file) and execute:
> .nuget\NuGet.exe pack Arena.Custom.SECC.Oauth.csproj -IncludeReferencedProjects

[Southeast Christian Church](http://www.southeastchristian.org/)
-----
Southeast Christian Church in Louisville, Kentucky is an evangelical Christian church. In our 
mission to connect people to Jesus and one another, Southeast Christian Church has 
grown into a unified multisite community located in the Greater Louisville/Southern Indiana region. 
We have multiple campuses serving the specific needs of the areas in which they are located, 
while receiving centralized leadership and teaching from the campus on Blankenbaker Parkway.