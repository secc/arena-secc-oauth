﻿USE [ArenaDB]

CREATE TABLE [dbo].[cust_secc_oauth_scope](
	[scope_id] [int] IDENTITY(1,1) NOT NULL,
	[scope_identifier] [varchar](25) NOT NULL,
	[scope_description] [varchar](max) NULL,
	[created_by] [varchar](50) NOT NULL,
	[modified_by] [varchar](50) NOT NULL,
	[date_created] [datetime] NOT NULL,
	[date_modified] [datetime] NOT NULL,
	[active] [bit] NOT NULL,
 CONSTRAINT [PK_cust_secc_oauth_scope] PRIMARY KEY CLUSTERED 
(
	[scope_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[cust_secc_oauth_scope] ADD  CONSTRAINT [DF_cust_secc_oauth_scope_active]  DEFAULT ((1)) FOR [active]
GO



CREATE TABLE [dbo].[cust_secc_oauth_client](
	[client_id] [int] IDENTITY(1,1) NOT NULL,
	[client_name] [varchar](50) NOT NULL,
	[api_key] [uniqueidentifier] NOT NULL,
	[api_secret] [uniqueidentifier] NOT NULL,
	[callback_url] [varchar](255) NULL,
	[created_by] [varchar](50) NOT NULL,
	[modified_by] [varchar](50) NOT NULL,
	[date_created] [datetime] NOT NULL,
	[date_modified] [datetime] NOT NULL,
	[active] [bit] NOT NULL,
 CONSTRAINT [PK_cust_secc_oauth_client] PRIMARY KEY CLUSTERED 
(
	[client_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[cust_secc_oauth_client] ADD  CONSTRAINT [DF_cust_secc_oauth_client_active]  DEFAULT ((1)) FOR [active]
GO

CREATE TABLE [dbo].[cust_secc_oauth_client_scope](
	[client_id] [int] NOT NULL,
	[scope_id] [int] NOT NULL,
	[created_by] [varchar](50) NOT NULL,
	[modified_by] [varchar](50) NOT NULL,
	[date_created] [datetime] NOT NULL,
	[date_modified] [datetime] NOT NULL,
	[active] [bit] NOT NULL,
 CONSTRAINT [PK_cust_secc_oauth_client_scope] PRIMARY KEY CLUSTERED 
(
	[client_id] ASC,
	[scope_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[cust_secc_oauth_client_scope] ADD  CONSTRAINT [DF_cust_secc_oauth_client_scope_active]  DEFAULT ((1)) FOR [active]
GO

ALTER TABLE [dbo].[cust_secc_oauth_client_scope]  WITH CHECK ADD  CONSTRAINT [FK_cust_secc_oauth_client_scope_cust_secc_oauth_client] FOREIGN KEY([client_id])
REFERENCES [dbo].[cust_secc_oauth_client] ([client_id])
GO

ALTER TABLE [dbo].[cust_secc_oauth_client_scope] CHECK CONSTRAINT [FK_cust_secc_oauth_client_scope_cust_secc_oauth_client]
GO

ALTER TABLE [dbo].[cust_secc_oauth_client_scope]  WITH CHECK ADD  CONSTRAINT [FK_cust_secc_oauth_client_scope_cust_secc_oauth_scope] FOREIGN KEY([scope_id])
REFERENCES [dbo].[cust_secc_oauth_scope] ([scope_id])
GO

ALTER TABLE [dbo].[cust_secc_oauth_client_scope] CHECK CONSTRAINT [FK_cust_secc_oauth_client_scope_cust_secc_oauth_scope]
GO

CREATE TABLE [dbo].[cust_secc_oauth_authorization](
	[authorization_id] [int] IDENTITY(1,1) NOT NULL,
	[client_id] [int] NOT NULL,
	[scope_id] [int] NOT NULL,
	[login_id] [varchar](50) NOT NULL,
	[date_created] [datetime] NOT NULL,
	[active] [bit] NOT NULL,
 CONSTRAINT [PK_cust_secc_oauth_authorization] PRIMARY KEY CLUSTERED 
(
	[authorization_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[cust_secc_oauth_authorization]  WITH CHECK ADD  CONSTRAINT [FK_cust_secc_oauth_authorization_cust_secc_oauth_client] FOREIGN KEY([client_id])
REFERENCES [dbo].[cust_secc_oauth_client] ([client_id])
GO

ALTER TABLE [dbo].[cust_secc_oauth_authorization] CHECK CONSTRAINT [FK_cust_secc_oauth_authorization_cust_secc_oauth_client]
GO

ALTER TABLE [dbo].[cust_secc_oauth_authorization]  WITH CHECK ADD  CONSTRAINT [FK_cust_secc_oauth_authorization_cust_secc_oauth_scope] FOREIGN KEY([scope_id])
REFERENCES [dbo].[cust_secc_oauth_scope] ([scope_id])
GO

ALTER TABLE [dbo].[cust_secc_oauth_authorization] CHECK CONSTRAINT [FK_cust_secc_oauth_authorization_cust_secc_oauth_scope]
GO

